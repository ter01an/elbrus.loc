<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Broadcast;
use App\Models\Room;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function __invoke(Room $room)
    {
        return view('pages.chat', [ 'room' => $room, 'user' => Auth::check() ? Auth::user() : false]);
    }
}
