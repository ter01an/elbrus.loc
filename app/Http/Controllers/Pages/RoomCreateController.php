<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Services\RoomService;
use Illuminate\Http\Request;

class RoomCreateController extends Controller
{
    public function __invoke(Request $request)
    {

        if($request->method() == 'POST') {

            RoomService::create($request->get('title'), $request->get('readonly', 0));

            return redirect('/cabinet/chats');
        }

        return view('cabinet.room_create');
    }
}
