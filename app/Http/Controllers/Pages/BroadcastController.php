<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Broadcast;
use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BroadcastController extends Controller
{
    public function __invoke(Broadcast $broadcast)
    {
        return view('pages.broadcast', ['broadcast' => $broadcast, 'room' => $broadcast->room, 'user' => Auth::check() ? Auth::user() : false]);
    }

    public function getReferral(Broadcast $broadcast)
    {
        return url("/login") . "?ref=" . Auth::user()->affiliate_id . "&redirect=/broadcast/$broadcast->id";
    }

    public function getQR(Broadcast $broadcast)
    {
        return QrCode::size(500)->generate(url("/login") . "?ref=" . Auth::user()->affiliate_id . "&redirect=/broadcast/$broadcast->id");
    }

    public function getList()
    {
        $items = Broadcast::all();

        return view('pages.broadcasts', ['items' => $items]);
    }
}
