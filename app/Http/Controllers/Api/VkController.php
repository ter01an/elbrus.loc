<?php

namespace App\Http\Controllers\Api;

use App\Services\UserService;
use App\Services\VkService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VkController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new VkService();
    }

    public function send(Request $request)
    {
        $userSocial = UserService::findSocialByUser($request->user);
        $this->client->send($userSocial->provider_id, $request->message);
        return redirect()->route('cabinet.users');
    }
}
