<?php

namespace App\Http\Controllers\Api\v1\Message;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Services\MessageService;

class DeleteController extends Controller
{
    public function __invoke(Message $message)
    {
        return [
            'success'   => !!MessageService::delete($message),
            'id'        => $message->id
        ];
    }
}
