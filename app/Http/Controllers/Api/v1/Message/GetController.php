<?php

namespace App\Http\Controllers\Api\v1\Message;

use App\Http\Controllers\Controller;
use App\Services\MessageService;
use Illuminate\Support\Facades\Auth;

class GetController extends Controller
{
    public function __invoke($chat_type, $chat_id)
    {
        $user = Auth::user();

        return MessageService::get($chat_id, $chat_type, ($user && $user->hasRole('root')));
    }
}
