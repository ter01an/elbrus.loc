<?php

namespace App\Http\Controllers\Api\v1\Room;

use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Services\RoomService;

class UserListController extends Controller
{
    public function __invoke(Room $room)
    {
        return RoomService::userList($room);
    }
}
