<?php

namespace App\Http\Controllers\Api\v1\Room;

use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Services\RoomService;

class DeleteController extends Controller
{
    public function __invoke(Room $room)
    {
        return [
            'success'   => !!RoomService::delete($room),
            'id'        => $room->id
        ];
    }
}
