<?php

namespace App\Http\Controllers\Api\v1\Room;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoomCreateRequest;
use App\Models\Room;
use App\Services\RoomService;

class CreateController extends Controller
{

    public function __invoke(RoomCreateRequest $request)
    {
        return [
            'room' => RoomService::create($request->get('title'), $request->get('readonly', 0))->only(['id', 'title', 'readonly'])
        ];
    }
}
