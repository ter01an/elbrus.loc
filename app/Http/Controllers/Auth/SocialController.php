<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserSocial;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $socialiteUser = Socialite::driver($provider)->user();
        $user = UserService::findUserBySocialId($provider, $socialiteUser->getId());

        if (isset($user->user_id)) {
            Auth::login(UserService::getUser($user->user_id), true);

            $redirect = request()->session()->get('redirect', '');
            if($redirect) {
                request()->session()->forget('redirect');
                return redirect($redirect);
            }
            return redirect()->route('cabinet.index');
        } else {
            if (!$user) {
                UserService::addSocialAccount($provider, $socialiteUser);
            }
            session(['social_id' => $socialiteUser->getId()]);
            return redirect()->route('register.index');
        }
    }

}
