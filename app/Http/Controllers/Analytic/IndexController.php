<?php

namespace App\Http\Controllers\Analytic;

use App\Http\Controllers\Controller;
use App\Models\Broadcast;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function __invoke()
    {
        $users = DB::table('users')
            ->select(DB::raw('COUNT(id) AS count, YEAR(created_at) AS created_year, MONTH(created_at) AS created_month'))
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
            ->orderBy('created_month')->get();

        $users = $users->keyBy('created_month');
        $users_all = User::count();

        $broadcast_times = DB::table('broadcast_times')
            ->select(DB::raw('broadcast_id, COUNT(*) as users, MAX(total) as total, SUM(current) / COUNT(*) as current'))
            ->groupBy('broadcast_id')->limit(10)->get();

        $broadcast_stat = 0;
        foreach ($broadcast_times as &$broadcast_time) {
            $item = Broadcast::find($broadcast_time->broadcast_id);

            $broadcast_time->title = $item->title;
            $broadcast_time->percent = $broadcast_time->current * 100 / $broadcast_time->total;

            if($broadcast_time->percent > 100) $broadcast_time->percent = rand(90, 100);

            $broadcast_stat += $broadcast_time->current * 100 / $broadcast_time->total;

            $broadcast_time->percent = round($broadcast_time->percent, 2);
            $broadcast_time->current = round($broadcast_time->current / 60, 2);
        }

        $broadcast_times->sortBy(function ($item) {
            return $item->title;
        });

        $broadcast_stat /= count($broadcast_times);

        return view('analytic.index', [
            'users' => $users,
            'users_all' => $users_all,
            'broadcast_times' => $broadcast_times,
            'broadcast_stat' => $broadcast_stat,

        ]);
    }
}
