<?php

namespace App\Http\Controllers\Generator;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GeneratorController extends Controller
{
    public function qr()
    {
        return QrCode::size(500)->generate(route('broadcast', 101));
    }

    public function ref()
    {
        return Auth::user()->getReferralLink();
    }
}
