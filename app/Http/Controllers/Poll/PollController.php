<?php

namespace App\Http\Controllers\Poll;

use App\Models\Poll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PollController extends Controller
{
    public function index()
    {
        return view('pages.poll');
    }

    public function result(Request $request)
    {
        $result = 0;
        foreach ($request->all() as $item) {
            $result += $item;
        }

        $poll = Poll::create([
            'user_id' => Auth::id(),
            'count' => $result
        ]);

        return view('pages.poll_result');
    }

}
