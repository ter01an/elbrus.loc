<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\BroadcastTimes;
use App\Models\Poll;
use App\Services\RoomService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CabinetUserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = UserService::getUser(Auth::id());
        $stats = [
            'polls' => Poll::where('user_id', Auth::id())->first(),
            'views' => BroadcastTimes::where('user_id', Auth::id())->first()
        ];
        return view('cabinet.index', compact('user', 'stats'));
    }

    public function users()
    {
        $users = UserService::getAllUsers();
        return view('cabinet.users', compact('users'));
    }

    public function user(Request $request)
    {
        $user = UserService::getUser($request->id);
        return view('user.index', compact('user'));
    }

    public function chats()
    {
        $rooms = RoomService::getAll();
        return view('cabinet.chats', compact('rooms'));
    }

    public function links()
    {
        return view('cabinet.links');
    }

}
