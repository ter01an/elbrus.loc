<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BroadcastTimes extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'broadcast_id', 'current', 'total'];
}
