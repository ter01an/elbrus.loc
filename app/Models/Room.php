<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'readonly'];

    public function messages() : Relation
    {
        return $this->morphMany(Message::class, 'chattable');
    }

    public function users() : Relation
    {
        return $this->belongsToMany(User::class, 'room_users');
    }
}
