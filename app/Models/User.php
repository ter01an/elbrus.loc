<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Questocat\Referral\Traits\UserReferral;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable, UserReferral;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeEmail($query, $value)
    {
        return $query->where('email', $value);
    }

    public function scopePhone($query, $value)
    {
        return $query->where('phone', $value);
    }

    public function messages() : Relation
    {
        return $this->morphMany(Message::class, 'chattable');
    }
}
