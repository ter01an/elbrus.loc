<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Broadcast extends Model
{
    protected $fillable = ['room_id', 'title', 'link', 'text', 'event_at'];

    protected $dates = ['event_at'];

    public function room() : Relation
    {
        return $this->belongsTo(Room::class);
    }
}
