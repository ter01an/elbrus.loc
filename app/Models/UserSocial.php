<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    protected $table = 'user_social';

    protected $fillable = [
        'nickname', 'email', 'avatar', 'user_id', 'provider', 'provider_id', 'token'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeEmail($query, $value)
    {
        return $query->where('email', $value);
    }

}
