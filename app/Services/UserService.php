<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserSocial;
use Illuminate\Support\Facades\Auth;

class UserService {

    public static function getUser($id)
    {
        return User::find($id);
    }

    public static function getAllUsers()
    {
        return User::all();
    }

    public static function create($request)
    {
        $userSocial = UserSocial::where('provider_id', $request->id)->first();
        $user = User::create([
            'avatar' => $userSocial->avatar,
            'name' => $userSocial->nickname,
            'email' => $userSocial->email,
            'phone' => $request->phone
        ]);
        $userSocial->user_id = $user->id;
        $userSocial->save();

        Auth::login($user, true);
        return redirect()->route('cabinet.index');
    }

    public static function findUserBySocialId($provider, $id)
    {
        $socialAccount = UserSocial::where('provider', $provider)
            ->where('provider_id', $id)->with('user')
            ->first();

        return $socialAccount ? $socialAccount : false;
    }

    public static function addSocialAccount($provider, $socialiteUser)
    {
        UserSocial::create([
            'avatar' => $socialiteUser->avatar,
            'nickname' => $socialiteUser->name,
            'email' => $socialiteUser->email,
            'provider' => $provider,
            'provider_id' => $socialiteUser->getId(),
            'token' => $socialiteUser->token,
        ]);
    }

    public static function findSocialByUser($id)
    {
        return UserSocial::where('user_id', $id)->first();
    }

    public static function getSocialType($id)
    {
        $userSocial = UserSocial::where('user_id', $id)->first();
        if ($userSocial) {
            return $userSocial->provider;
        }
        return false;
    }

}
