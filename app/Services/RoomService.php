<?php

namespace App\Services;

use App\Events\NewRoomJoinEvent;
use App\Models\Room;
use App\Models\RoomUser;
use App\Models\User;

class RoomService
{
    public static function getAll()
    {
        return Room::all();
    }

    public static function userList(Room $room) : iterable
    {
        return $room->users->map(function ($item) {
            return $item->only(['id', 'name', 'avatar']);
        });
    }

    public static function join(Room $room, $user) : RoomUser
    {
        $result = RoomUser::updateOrCreate([
            'room_id' => $room->id,
            'user_id' => $user->id
        ]);

        event(new NewRoomJoinEvent($room, $user));

        return $result;
    }

    public static function leave(Room $room, $user) : bool
    {
        return RoomUser::whereColumn([
            'room_id' => $room->id,
            'user_id' => $user->id
        ])->delete();
    }

    public static function create(string $title, int $readonly = 0) : Room
    {
        return Room::create(['title' => $title, 'readonly' => $readonly]);
    }

    public static function delete(Room &$room) : bool
    {
        try {
            return $room->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function update(Room &$room, string $title, int $readonly = 0) : bool
    {
        return $room->update(['title' => $title, 'readonly' => $readonly]);
    }
}
