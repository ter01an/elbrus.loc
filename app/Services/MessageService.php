<?php

namespace App\Services;

use App\Events\NewMessageEvent;
use App\Exceptions\ApiException;
use App\Models\Message;
use App\Models\Room;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class MessageService
{
    public static function get(int $chat_id, string $chat_type, $with_trashed = false) : iterable
    {
        $instance = self::getTypeModel($chat_type);

        if($instance)
        {
            $instance = $instance->find($chat_id);
        }

        if(!$instance)
        {
            throw new ApiException('Chat not found');
        }

        $messages = $instance->messages()->orderBy('created_at', 'asc');

        if($with_trashed)
        {
            $messages->withTrashed();
        }

        return $messages->get()->map(function ($item) {
            return self::info($item);
        });
    }

    public static function create($user, int $to_id, string $to_type, string $text) : Message
    {
        $instance = self::getTypeModel($to_type);

        if($instance)
        {
            $instance = $instance->find($to_id);
        }

        if(!$instance)
        {
            throw new ApiException('Recipient not found');
        }

        $message = Message::create([
            'user_id'   => $user->id,
            'text'      => $text
        ]);

        $instance->messages()->save($message);

        event(new NewMessageEvent($to_id, $to_type, $message));

        return $message;
    }

    public static function delete(Message $message)
    {
        try {
            return $message->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getTypeModel(string $type)
    {
        switch ($type)
        {
            case 'room':
                return new Room();
            case 'user':
                return new User();
            default:
                return false;
        }
    }

    public static function getTypeTitle(Model $type)
    {
        switch ($type)
        {
            case $type instanceof Room:
                return 'room';
            case $type instanceof User:
                return 'user';
            default:
                return false;
        }
    }

    public static function info(Message $message)
    {
        return [
            'id'            => $message->id,
            'from_id'       => $message->user_id,
            'to_id'         => $message->chattable->id,
            'to_type'       => self::getTypeTitle($message->chattable),
            'text'          => $message->text,
            'created_at'    => strtotime($message->created_at),
            'deleted'       => !!$message->deleted_at
        ];
    }
}
