<?php

namespace App\Events;

use App\Models\Message;
use App\Models\Room;
use App\Services\MessageService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $to_type;
    protected $to_id;
    protected $message;

    /**
     * Create a new event instance.
     *
     * @param int $to_id
     * @param string $to_type
     * @param Message $message
     */
    public function __construct(int $to_id, string $to_type, Message $message)
    {
        $this->to_type = $to_type;
        $this->to_id = $to_id;
        $this->message = MessageService::info($message);
    }

    public function broadcastWith()
    {
        return $this->message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("{$this->to_type}.{$this->to_id}");
    }

    public function broadcastAs()
    {
        return 'Message.New';
    }
}
