@extends('layouts.cabinet')

@section('content')
    <div class="container py-5">
        <div class="card">
            <div class="card-header">Отправить сообщение (соц. сеть)</div>
            <div class="card-body">
                <form action="{{ route('api.vk.send') }}" class="row">
                    <div class="form-group col-12 col-md-5 col-lg-4">
                        <textarea class="form-control" name="message" cols="30" rows="10"></textarea>
                        <input type="hidden" name="user" value="{{ $user->id }}">
                    </div>
                    <div class="form-group col-12">
                        <button class="btn btn-success">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
