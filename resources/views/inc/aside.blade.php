<aside>
    <ul>
        @if(\Illuminate\Support\Facades\Auth::check())
            <li><a title="ЛК" href="{{ route('cabinet.index') }}"><img src="{{ asset('/images/aside/user-profile.svg') }}" alt="">Кабинет</a></li>
            <li><a title="Пользователи" href="{{ route('cabinet.users') }}"><img src="{{ asset('/images/aside/users.svg') }}" alt="">Пользв.</a></li>
            <li><a title="Чат" href="{{ route('cabinet.chats') }}"><img src="{{ asset('/images/aside/chat.svg') }}" alt="">Беседы</a></li>
            <li><a title="Статистика" href="/analytic"><img src="{{ asset('/images/aside/statistics.svg') }}" alt="">Статистика</a></li>
            <li><a title="Реф. ссылка" href="{{ route('cabinet.links') }}"><img src="{{ asset('/images/aside/link.svg') }}" alt="">Рефер</a></li>
            <li><a title="Курсы" href="{{ route('broadcast.list') }}"><img src="{{ asset('/images/aside/broadcasting.svg') }}" alt="">Курсы</a></li>
            <li><a title="Тесты" href="{{ route('poll') }}"><img src="{{ asset('/images/aside/test.svg') }}" alt="">Тесты</a></li>
        @endif
    </ul>
</aside>
