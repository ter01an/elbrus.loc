@extends('layouts.cabinet')

@section('content')
    <div class="container py-5">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12 mb-5">
                    <h5>Всего пользователей в системе: {{ $users_all }}</h5>

                    <canvas data-items="{{ json_encode($users) }}" id="charts-users" style="width: 100%; height: 200px;"></canvas>
                </div>
                <div class="col-md-12 mb-5">
                    <h5>Средняя продолжительность просмотра последних трансляций: {{ round($broadcast_stat, 2) }}%</h5>

                    <canvas data-items="{{ json_encode($broadcast_times) }}" id="charts-broadcast" style="width: 100%; height: 400px;"></canvas>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ mix('js/analytic.js') }}"></script>
@endsection
