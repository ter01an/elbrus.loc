@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div class="card">
            <div class="card-header">Тестирование</div>
            <div class="card-body">
                <form action="{{ route('poll.result') }}">
                    <div class="form-group">
                        <label class="font-weight-bolder">2 + 2?</label>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="param1" value="0" checked="checked">
                            <label class="form-check-label">5</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="param1" value="0">
                            <label class="form-check-label">3</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="param1" value="1">
                            <label class="form-check-label">4</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bolder">Сколько букв в слове "Эльбрус"?</label>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="param2" value="0" checked="checked">
                            <label class="form-check-label">5</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="param2" value="0">
                            <label class="form-check-label">9</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="param2" value="1">
                            <label class="form-check-label">Нет правильного ответа</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
