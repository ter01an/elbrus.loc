@extends('layouts.app')

@section('content')

    <div class="container py-2">
        <h1>{{ $room->title }}</h1>
        <div id="chat-room" data-room="{{ $room->id }}" data-user="{{ $user ? $user->id : "" }}"></div>
    </div>

    <link rel="stylesheet" href="{{ mix('css/chats.css') }}">
    <script src="{{ mix('js/chats.js') }}"></script>
@endsection
