@extends('layouts.cabinet')

@section('content')
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body py-5">
                        <h5>Курсы</h5>
                        <table class="table">
                            <thead>
                            <th scope="col"></th>
                            <th scope="col">Название</th>
                            <th scope="col">Онлайн/офлайн</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr class="bg-white">
                                    <td></td>
                                    <td>{{ $item->title }}</td>
                                    <td>
                                        @if($item->online === 0)
                                            <div class="btn btn-warning">Офлайн</div>
                                        @else
                                            <div class="btn btn-success">Онлайн</div>
                                        @endif
                                    </td>
                                    <td>
                                        <div style="display: none" id="ref-{{ $item->id }}">{{ url("/login") . "?ref=" . \Illuminate\Support\Facades\Auth::user()->affiliate_id . '&redirect=/broadcast/' . $item->id }}</div>
                                        <a href="#" class="btn btn-primary copy-ref-broadcast" data-target="ref-{{ $item->id }}">Реферальная ссылка</a>
                                        <a href="/broadcast/qr/{{ $item->id }}" class="btn btn-primary" target="_blank"><i class="fas fa-qrcode"></i></a>
                                    </td>
                                    <td>
                                        <a href="/broadcast/{{ $item->id }}" target="_blank">Перейти на страницу</a>
                                    </td>
                                    <td><a href="{{ route('broadcast.edit', $item->id) }}" class="btn btn-sm btn-outline-primary">Статистика</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
