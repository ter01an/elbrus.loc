@extends('layouts.app')

@section('content')

    <div class="b-online-courses b-default-courses">
        <div class="container">
            <h3>Онлайн-курсы</h3>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('chat') }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_1</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('chat') }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_2</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('chat') }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_3</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('chat') }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_4</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="b-projects b-default-courses">
        <div class="container">
            <h3>Проекты</h3>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="item">
                        <img src="{{ asset('/images/default-project.jpg') }}" alt="">
                        <span>Проект_1</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="item">
                        <img src="{{ asset('/images/default-project.jpg') }}" alt="">
                        <span>Проект_2</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="item">
                        <img src="{{ asset('/images/default-project.jpg') }}" alt="">
                        <span>Проект_3</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="b-offline-courses b-default-courses">
        <div class="container">
            <h3>Оффлайн-курсы</h3>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('broadcast', 101) }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_1</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('broadcast', 102) }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_2</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('broadcast', 103) }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_3</span>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="{{ route('broadcast', 104) }}" class="item">
                        <img src="{{ asset('/images/default-course.jpg') }}" alt="">
                        <span>Курс_4</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
