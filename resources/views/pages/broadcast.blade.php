@extends('layouts.app')

@section('content')

    <div class="container py-2">
        <h1>{{ $broadcast->title }}</h1>

        @if($broadcast->online === 0)
            <video class="broad-iframe" src="https://s3-lms.rsv.ru/img/b7e4260180657990c55c3f86d2e14b094b49275e9bb9c7f18d6fb79fcd363d31.mp4#t=0" preload="auto" controls="" controlslist="nodownload" style="width: 100%; height: 100%;"></video>
        @else
            <iframe class="broad-iframe" width="100%" height="625px" src="https://player.twitch.tv/?channel=ter01an&controls=false&migration=true&parent=2chairs.fun&referrer=https%3A%2F%2F2chairs.fun%2Fchat" frameborder="0"></iframe>

            <div id="chat-room" data-room="{{ $room->id }}" data-user="{{ $user ? $user->id : "" }}"></div>

            <link rel="stylesheet" href="{{ mix('css/chats.css') }}">
            <script src="{{ mix('js/chats.js') }}"></script>
        @endif

        <div class="card pt-3">
            <div class="card-body">
                {!! $broadcast->text !!}
            </div>
        </div>
    </div>

@endsection
