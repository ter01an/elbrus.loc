@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card my-5">
                    <div class="card-header">Завершение регистрации</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('register.create') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">Телефон</label>
                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control mask-phone" data-mask="(000) 000-0000" name="phone" required autocomplete="name" autofocus>
                                    <input type="hidden" name="id" value="{{ session()->get('social_id') }}">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-success">Завершить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
