@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card my-5">
                <div class="card-header">
                    Авторизация
                </div>
                <div class="card-body">
                    <a href="{{ route('auth.social', 'vkontakte') }}" class="social-icon" title="vkontakte">
                        <img src="{{ asset('/images/social/vk.svg') }}" alt="">
                    </a>
                    <a href="#" title="telegram" class="social-icon social-disabled">
                        <img src="{{ asset('/images/social/telegram.svg') }}" alt="">
                    </a>
                    <a href="#" title="instagram" class="social-icon social-disabled">
                        <img src="{{ asset('/images/social/instagram-2016.svg') }}" alt="">
                    </a>
                    <a href="#" title="whatsapp" class="social-icon social-disabled">
                        <img src="{{ asset('/images/social/whatsapp-icon.svg') }}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
