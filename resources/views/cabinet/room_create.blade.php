@extends('layouts.cabinet')

@section('content')
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body py-5">
                        <h5>Создать беседу</h5>
                        <form method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-12 col-md-3">Имя беседы</label>
                                <div class="col-12 col-md-6 col-xl-5">
                                    <input id="name" required type="text" class="form-control" value="" name="title">
                                </div>
                            </div>

                            <div class="form-group row mt-5 mb-0">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
