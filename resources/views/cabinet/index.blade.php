@extends('layouts.cabinet')

@section('content')
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body py-5">
                        <form action="{{ route('cabinet.index') }}" type="post">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-12 col-md-3">Имя</label>
                                <div class="col-12 col-md-6 col-xl-5">
                                    <input id="name" type="text" class="form-control" value="{{ $user->name }}" name="name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-12 col-md-3">Email</label>
                                <div class="col-12 col-md-6 col-xl-5">
                                    <input id="email" type="email" class="form-control" value="{{ $user->email }}" name="email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-12 col-md-3">Телефон</label>
                                <div class="col-12 col-md-6 col-xl-5">
                                    <input id="phone" type="text" class="form-control" value="{{ $user->phone }}" name="phone">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-md-3">Кол-во правильных ответов:</label>
                                <div class="col-12 col-md-6 col-xl-5 font-weight-bolder">
                                    {{ isset($stats['polls']->count) ? $stats['polls']->count : 0 }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-md-3">Кол-во просмотров:</label>
                                <div class="col-12 col-md-6 col-xl-5 font-weight-bolder">
                                    {{ isset($stats['views']->current) ? $stats['views']->current : 0 }}
                                </div>
                            </div>

                            <div class="form-group row mt-5 mb-0">
                                <div class="col-md-6">
                                    <div id="vk_send_message"></div>
                                    <script type="text/javascript">
                                        VK.Widgets.AllowMessagesFromCommunity("vk_send_message", {height: 30}, 199717637);
                                    </script>
                                </div>
                            </div>

                            <div class="form-group row mt-5 mb-0">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
