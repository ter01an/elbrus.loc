@extends('layouts.cabinet')

@section('content')
    <div class="container py-5">
        <div class="card">
            <div class="card-body py-3">
                <div id="referal-url" style="display:none;">{{ \Illuminate\Support\Facades\Auth::user()->getReferralLink() }}</div>
                <a href="{{ route('generator.ref') }}" target="_blank" class="copy-ref btn btn-success mt-1 d-inline-block">Генерировать реф. ссылку</a>
                <a href="{{ route('generator.qr') }}" target="_blank" class="btn btn-success mt-1 d-inline-block">Генерировать QR-code</a>
            </div>
        </div>
    </div>
@endsection
