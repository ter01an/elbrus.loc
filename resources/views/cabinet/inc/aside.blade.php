<aside>
    <ul>
        <li><a title="ЛК" href="{{ route('cabinet.index') }}"><img src="{{ asset('/images/aside/user-profile.svg') }}" alt=""></a></li>
        <li><a title="Пользователи" href="{{ route('cabinet.users') }}"><img src="{{ asset('/images/aside/users.svg') }}" alt=""></a></li>
        <li><a title="Диалоги" href="{{ route('cabinet.chats') }}"><img src="{{ asset('/images/aside/chat.svg') }}" alt=""></a></li>
        <li><a title="Статистика" href="/analytic"><img src="{{ asset('/images/aside/statistics.svg') }}" alt=""></a></li>
        <li><a title="Реф. ссылка" href="{{ route('cabinet.links') }}"><img src="{{ asset('/images/aside/link.svg') }}" alt=""></a></li>
        @if(\Illuminate\Support\Facades\Auth::check())
        <li><a title="Трансляции и курсы" href="{{ route('broadcast.list') }}"><img src="{{ asset('/images/aside/broadcasting.svg') }}" alt=""></a></li>
        @endif
    </ul>
</aside>
