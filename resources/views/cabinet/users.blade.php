@extends('layouts.cabinet')

@section('content')
    <div class="container py-5">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <th scope="col"></th>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Телефон</th>
                <th scope="col"></th>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr class="bg-white">
                        <td style="min-width: 50px;">
                            @if (\App\Services\UserService::getSocialType($user->id) == 'vkontakte')
                                <img src="{{ asset('/images/social/vk.svg') }}" alt="" width="25">
                            @endif
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td style="min-width: 50px;"><a href="{{ route('users.index', $user->id) }}" class=""><img src="{{ asset('/images/send-icon.svg') }}" alt="" width="25"></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
