@extends('layouts.cabinet')

@section('content')
    <div class="container py-5">
        <div class="panel text-right py-3">
            <a href="/room/create" class="btn btn-success">Создать канал</a>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <th scope="col">Название</th>
                <th scope="col"></th>
                </thead>
                <tbody>
                @foreach($rooms as $room)
                    <tr class="bg-white">
                        <th>{{ $room->title }}</th>
                        <th class="text-right"><a href="/chat/{{ $room->id }}"><img src="{{ asset('/images/edit-icon.svg') }}" alt="" width="25"></a></th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
