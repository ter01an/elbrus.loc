@extends('layouts.app')

@section('content')

    <div class="container py-2">
        <iframe class="broad-iframe" width="100%" height="625px" src="https://player.twitch.tv/?channel=ter01an&controls=false&migration=true&parent=2chairs.fun&referrer=https%3A%2F%2F2chairs.fun%2Fchat" frameborder="0"></iframe>

        <div id="chat-room" data-room="1" data-user="{{ \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : "" }}"></div>
    </div>

    <link rel="stylesheet" href="{{ mix('css/chats.css') }}">
    <script src="{{ mix('js/chats.js') }}"></script>
@endsection
