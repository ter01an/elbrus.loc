
class ChatApi {

    async joinToRoom(room_id, user_id) {
        const response = await fetch(`/api/v1/room/${room_id}/${user_id}/join`, {
            headers: {
                'Accept': 'application/json'
            },
            method: 'POST',
            credentials: 'include'
        });
        return await response.json();
    }
}

export default new ChatApi();
