import React, { createContext, useContext, useState, useEffect } from 'react'
import ChatApi from "../ChatApi";
import useRoomUsers from "../stores/useRoomUsers";

const ChatStateContext = createContext();

function ChatStateProvider(props) {
    const [room, setRoom] = useState(props.room);
    const [user, setUser] = useState(props.user);
    const [done, setDone] = useState(false);
    const {mutateUsers} = useRoomUsers(room);

    useEffect(async () => {
        if(user) {
            await ChatApi.joinToRoom(room, user);
            mutateUsers();
        }
        setDone(true);
    }, [room, user])

    return (
        <ChatStateContext.Provider value={{ room, user }}>{done && props.children}</ChatStateContext.Provider>
    )
}

const useChatState = () => useContext(ChatStateContext)

export { ChatStateProvider, useChatState }
