import React, { useEffect, useState } from "react";
import { useChatState } from "./ChatState";
import moment from "moment";
moment.locale('ru');

export default function ChatMessage({ message, users }) {
    const { user } = useChatState();
    const [ date, setDate ] = useState();

    function getDate() {
        return moment.duration(moment().diff(message.created_at * 1000)).hours() >= 1 ?
            moment(message.created_at * 1000).format('DD MMMM YYYY в HH:ss') :
            moment(message.created_at * 1000).fromNow();
    }

    useEffect(() => {
        setDate(getDate())

        let interval = setInterval(() => {
            setDate(getDate())
        }, 10000);

        return () => {
            clearInterval(interval);
        }
    }, [])

    return (
        <div className={"chat-message " + (user != message.from_id ? "left" : "right")}>
            <img className="message-avatar" src={users[message.from_id].avatar ? users[message.from_id].avatar : "/images/default-avatar.png"}/>
            <div className="message">
                <a className="message-author">{users[message.from_id].name}</a>
                <span className="message-date">{date}</span>
                <span className="message-content">
                    {message.text}
                </span>
            </div>
        </div>
    )
}
