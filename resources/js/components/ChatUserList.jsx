import React from 'react';
import useRoomUsers from "../stores/useRoomUsers";
import { useChatState } from "./ChatState";
import { useEffect } from "react";
import {find} from "lodash";

export default function ChatUserList() {
    const { room, user } = useChatState();
    const { users, mutateUsers } = useRoomUsers(room);

    useEffect(() => {
        Echo.channel(`room.join.${room}`)
        .listen('.Join.New', async (e) => {
            mutateUsers(async list => {
                if(!find(list, item => item.id == e.id)) {
                    list.push(e);
                }

                return list
            })
        });
    }, []);


    if(typeof users === "undefined") {
        return (
            <div className="chat-users loading"/>
        )
    }

    return (
        <div className="chat-users">
            <div className="users-list">
                {users.map(item => (
                    <div key={item.id} className="chat-user">
                        <img className="chat-avatar" src={item.avatar ? item.avatar : "/images/default-avatar.png"}/>
                        <div className="chat-user-name">
                            <a>{item.name}</a>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}
