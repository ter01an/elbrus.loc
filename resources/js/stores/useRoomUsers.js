import useSWR from 'swr'

export default function useRoomUsers (room_id) {
    const { data, error, isValidating, mutate } = useSWR(`/api/v1/room/${room_id}/users`)

    return {
        users: !error ? data : false,
        mutateUsers: mutate,
        isLoading: isValidating,
        isError: error
    }
}
