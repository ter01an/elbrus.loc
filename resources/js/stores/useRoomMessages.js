import useSWR from 'swr'

export default function useRoomMessages (room_id) {
    const { data, error, isValidating, mutate } = useSWR(`/api/v1/message/room/${room_id}`)

    return {
        messages: !error ? data : false,
        mutateMessages: mutate,
        isLoading: isValidating,
        isError: error
    }
}
