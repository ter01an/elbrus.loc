import Chart from 'chart.js';
import each from "lodash/each";

var MONTHS = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

let chartUsers = document.getElementById('charts-users');
let dataUsers = JSON.parse(chartUsers.dataset.items);

let dataUserList = [];
each(MONTHS, (m, i) => {
    if(i in dataUsers) {
        dataUserList.push(dataUsers[i].count);
    } else {
        dataUserList.push(0)
    }
})

var config = {
    type: 'line',
    data: {
        labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        datasets: [{
            label: 'Новые пользователи',
            backgroundColor: "#C8E7B7",
            borderColor: "#C8E7B7",
            data: dataUserList,
            fill: false,
        }]
    },
    options: {
        responsive: true,
        title: {
            display: false,
            text: 'Chart.js Line Chart'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Value'
                }
            }]
        }
    }
};

var ctx = chartUsers.getContext('2d');
window.myLine = new Chart(ctx, config);


/* Broadcast items */


let chartBroadcast = document.getElementById('charts-broadcast');
let dataTimes = JSON.parse(chartBroadcast.dataset.items);

let labels = [];
let data = [], data2 = [], data3 = [];

each(dataTimes, time => {
    labels.push(time.title);
    data.push(time.percent);
    data2.push(time.users);
    data3.push(time.current);
})

var barChartData = {
    labels: labels,
    datasets: [{
        label: 'Среднее время просмотра в %',
        backgroundColor: "#C8E7B7",
        borderColor: "#C8E7B7",
        borderWidth: 1,
        data: data
    }, {
        label: 'Среднее время просмотра в мин.',
        backgroundColor: "#3BA58B",
        borderColor: "#3BA58B",
        borderWidth: 1,
        data: data3
    }, {
        label: 'Количетсво участников',
        backgroundColor: "#2F4764",
        borderColor: "#2F4764",
        borderWidth: 1,
        data: data2
    }]
};

var ctx2 = chartBroadcast.getContext('2d');
window.myBar = new Chart(ctx2, {
    type: 'bar',
    data: barChartData,
    options: {
        responsive: true,
        legend: {
            position: 'top',
        },
        title: {
            display: false,
            text: 'Chart.js Bar Chart'
        }
    }
});

