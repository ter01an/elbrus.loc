<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'referral'], function (){
    Auth::routes();

    Route::get('/test', function (){
        return view('chattest');
    });

    Route::group(['prefix' => 'analytic', 'namespace' => 'Analytic'], function (){
        Route::get('/', 'IndexController');
    });

    Route::any('/room/create', 'Pages\RoomCreateController');

    Route::get('/broadcast/list', 'Pages\BroadcastController@getList')->name('broadcast.list');
    Route::get('/broadcast/edit/{broadcast}', 'Pages\BroadcastController@edit')->name('broadcast.edit');
    Route::get('/broadcast/ref/{broadcast}', 'Pages\BroadcastController@getReferral')->name('broadcast.ref');
    Route::get('/broadcast/qr/{broadcast}', 'Pages\BroadcastController@getQR')->name('broadcast.qr');
    Route::get('/broadcast/{broadcast}', 'Pages\BroadcastController')->name('broadcast');
    Route::get('/chat/{room}', 'Pages\ChatController')->name('chat');

    /* Social auth */
    Route::group(['prefix' => 'social-auth'], function (){
        Route::get('{provider}', 'Auth\SocialController@redirectToProvider')->name('auth.social');
        Route::get('{provider}/callback', 'Auth\SocialController@handleProviderCallback')->name('auth.social.callback');
    });
    /* Social auth */

    /* Social API */
    Route::group(['prefix' => 'social-api'], function (){
        Route::get('vk-send', 'Api\VkController@send')->name('api.vk.send');
    });
    /* Social API */

    /* Registration */
    Route::group(['prefix' => 'register'], function (){
        Route::get('', 'Auth\RegisterController@index')->name('register.index');
        Route::get('/referral', 'Auth\RegisterController@referral')->name('register.referral');
        Route::post('/create', 'Auth\RegisterController@create')->name('register.create');
    });
    /* Registration */

    /* Cabinet */
    Route::group(['prefix' => 'cabinet'], function (){
        Route::get('index', 'Cabinet\CabinetUserController@index')->name('cabinet.index');
        Route::get('users', 'Cabinet\CabinetUserController@users')->name('cabinet.users');
        Route::get('chats', 'Cabinet\CabinetUserController@chats')->name('cabinet.chats');
        Route::get('statistics', 'Cabinet\CabinetUserController@statistics')->name('cabinet.statistics');
        Route::get('links', 'Cabinet\CabinetUserController@links')->name('cabinet.links');
    });
    /* Cabinet */

    /* Generate */
    Route::group(['prefix' => 'generator'], function (){
        Route::get('qr', 'Generator\GeneratorController@qr')->name('generator.qr');
        Route::get('ref', 'Generator\GeneratorController@ref')->name('generator.ref')->middleware('referral');
    });
    /* Generate */

    Route::get('', 'Pages\PageController@index')->name('index');
    Route::get('/poll', 'Pages\PageController@poll')->name('poll');

    Route::group(['prefix' => 'users', 'middleware' => 'auth'], function () {
        Route::get('{id}', 'Cabinet\CabinetUserController@user')->name('users.index');
    });

    Route::group(['prefix' => 'poll', 'middleware' => 'auth'], function () {
        Route::get('/', 'Poll\PollController@index')->name('poll');
        Route::get('result', 'Poll\PollController@result')->name('poll.result');
    });


    Route::get('/chat', function (){
        return view('chattest');
    })->name('chat');
});
