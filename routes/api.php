<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'v1', 'prefix' => 'v1'], function () {
        Route::group(['namespace' => 'Room', 'prefix' => 'room'], function () {
            Route::get('/{room}/users', 'UserListController');
            Route::post('/', 'CreateController');
            Route::post('/{room}', 'UpdateController');
            Route::post('/{room}/{user}/join', 'JoinController');
            Route::delete('/{room}', 'DeleteController');
        });
        Route::group(['namespace' => 'Message', 'prefix' => 'message'], function () {
            Route::get('/{type}/{id}', 'GetController');
            Route::post('/', 'CreateController');
            Route::delete('/{message}', 'DeleteController');
        });
    });
});
