<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Broadcast;
use Faker\Generator as Faker;

$factory->define(Broadcast::class, function (Faker $faker) {
    return [
        'room_id' => function() {
            return factory(\App\Models\Room::class)->create()->id;
        },
        'title' => $faker->name,
        'link' => 'https://player.twitch.tv/?channel=ter01an&controls=false&migration=true&parent=2chairs.fun&referrer=https%3A%2F%2F2chairs.fun%2Fchat',
        'text' => $faker->realText(),
        'event_at' => $faker->dateTime(),
    ];
});
