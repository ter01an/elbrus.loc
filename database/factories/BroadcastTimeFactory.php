<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\BroadcastTimes::class, function (Faker $faker) {
    $total = rand(30 * 60, 120 * 60);
    $current = abs($total - rand(30 * 60, 120 * 60));
    return [
        'user_id' => function() {
            return \App\Models\User::inRandomOrder()->first()->id;
        },
        'broadcast_id' => function() {
            return \App\Models\Broadcast::inRandomOrder()->first()->id;
        },
        'current' => $current,
        'total' => $total
    ];
});
