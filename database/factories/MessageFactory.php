<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Message::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return \App\Models\User::inRandomOrder()->first()->id;
        },
        'chattable_id' => function() {
            return \App\Models\Room::inRandomOrder()->first()->id;
        },
        'chattable_type' => \App\Models\Room::class,
        'text' => $faker->realText(50)
    ];
});
