<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\RoomUser::class, function (Faker $faker) {
    $room_id = \App\Models\Room::inRandomOrder()->first()->id;;
    $user_id= \App\Models\User::inRandomOrder()->first()->id;

    if(\App\Models\RoomUser::where('room_id', $room_id)->where('user_id',$user_id)->count()) return false;

    return [
        'room_id' => $room_id,
        'user_id' => $user_id
    ];
});
