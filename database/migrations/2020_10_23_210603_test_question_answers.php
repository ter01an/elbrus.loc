<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestQuestionAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_question_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('test_question_id')->unsigned()->index();
            $table->string('title');
            $table->tinyInteger('correct');
            $table->integer('order')->index()->default(0);

            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->foreign('test_question_id')
                ->references('id')
                ->on('test_questions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_question_answers');
    }
}
